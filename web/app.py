from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def index():
   return render_template("name.html"),200

@app.route("/<path:name>")
def hello(name):
       try: 
              return render_template(name),200
       except:
              if "//" in name or "~" in name or ".." in name:
                  return page_is_forbidden(403)
       
              else:
                  return page_not_found(404)
 
@app.errorhandler(404)
def page_not_found(error):
   return render_template('404.html'),404

@app.errorhandler(403)
def page_is_forbidden(error):
   return render_template('403.html'),403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
