A "getting started" manual for Dockers. CIS 322, Introduction to Software Engineering, at the University of Oregon. Reference: https://docs.docker.com/engine/reference/builder/

Clone this repo to your laptop/Desktop

Then follow the instructions below to build using docker with the included Dockerfile:

1)Go to the web folder in the repository. Read every line of the docker file and the simple flask app.

2)Build the simple flask app image using

  ```
  docker build -t uocis-flask-demo .
  ```
  
  Run the container using
  
  ```
  docker run -d -p 5000:5000 UOCIS-flask-demo
  ```
  
3)Launch localhost:5000 using web broweser and check the output

should be an offset Hello on an Orange page - which was set as the index page

The pageserver has been altered to return 404 "File is not found" error if the page is not there. 
It will throw a 403 "File is forbbiden" error if the URL contains // ~ or .. within it. And if it is a webpage 
within the templetes for the server. the page will be displayed with the correct formatting(css) if URL is entered correctly.